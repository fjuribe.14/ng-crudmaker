"use strict";var _express=_interopRequireDefault(require("express")),_path=_interopRequireDefault(require("path")),_dotenv=_interopRequireDefault(require("dotenv")),_ejs=_interopRequireDefault(require("ejs")),_index=_interopRequireDefault(require("./routes/index.route"));function _interopRequireDefault(a){return a&&a.__esModule?a:{default:a}}// Init
var app=(0,_express["default"])();// Settings
// Middlewares
// Routes
// Static
// Server
_dotenv["default"].config(),app.set("port",process.env.PORT),app.engine("html",_ejs["default"].renderFile),app.set("view engine","html"),app.set("views",_path["default"].join(__dirname,"public")),app.use(_express["default"].urlencoded({extended:!1})),app.use(_express["default"].json()),app.use("/",_index["default"]),app.get("*",function(a,b,c){b.status(404).json("not found 404"),c()}),app.use(_express["default"]["static"](_path["default"].join(__dirname,"public"))),app.listen(app.get("port"),function(){console.log("server on port: ".concat(app.get("port")))});