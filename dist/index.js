"use strict";var _axios=_interopRequireDefault(require("axios"));function _interopRequireDefault(a){return a&&a.__esModule?a:{default:a}}function asyncGeneratorStep(a,b,c,d,e,f,g){try{var h=a[f](g),i=h.value}catch(a){return void c(a)}h.done?b(i):Promise.resolve(i).then(d,e)}function _asyncToGenerator(a){return function(){var b=this,c=arguments;return new Promise(function(d,e){function f(a){asyncGeneratorStep(h,d,e,f,g,"next",a)}function g(a){asyncGeneratorStep(h,d,e,f,g,"throw",a)}var h=a.apply(b,c);f(void 0)})}}/**
 * @method firstMethod
 * @param {string} text any text do yo want
 * @returns {string}
 */var firstMethod=function(){return"hello world"},getUsers=/*#__PURE__*/function(){var a=_asyncToGenerator(/*#__PURE__*/regeneratorRuntime.mark(function a(){return regeneratorRuntime.wrap(function(a){for(;;)switch(a.prev=a.next){case 0:return a.next=2,_axios["default"].get("http://jsonplaceholder.typicode.com/users").then(function(a){return res.json(a.data)})["catch"](function(a){return console.error(a)});case 2:return a.abrupt("return",a.sent);case 3:case"end":return a.stop();}},a)}));return function(){return a.apply(this,arguments)}}(),getUserById=/*#__PURE__*/function(){var a=_asyncToGenerator(/*#__PURE__*/regeneratorRuntime.mark(function a(b){return regeneratorRuntime.wrap(function(a){for(;;)switch(a.prev=a.next){case 0:return a.next=2,_axios["default"].get("http://jsonplaceholder.typicode.com/users/".concat(b)).then(function(a){return res.json(a.data)})["catch"](function(a){return console.error(a)});case 2:return a.abrupt("return",a.sent);case 3:case"end":return a.stop();}},a)}));return function(){return a.apply(this,arguments)}}();/**
 * Obtener todos los usuarios
 * @method getUsers
 * @async
 * @returns {Object} retorna usuarios
 */