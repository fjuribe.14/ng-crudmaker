import axios from "axios";

const indexCtrl = {
  index: (req, res) => {
    return res.render("index");
  },
  getUsers: async (req, res) => {
    return await axios
      .get(`http://jsonplaceholder.typicode.com/users`)
      .then((response) => res.json(response.data))
      .catch((err) => console.error(err));
  },
  getUserById: async (req, res) => {
    return await axios
      .get(`http://jsonplaceholder.typicode.com/users/${req.params.id}`)
      .then((response) => res.json(response.data))
      .catch((err) => console.error(err));
  },
};

export default indexCtrl;
