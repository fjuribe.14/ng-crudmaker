import express from "express";
import path from "path";
import dotenv from "dotenv";
import ejs from "ejs";

import indexRoutes from "./routes/index.route";

// Init
const app = express();
dotenv.config();

// Settings
app.set("port", process.env.PORT);
app.engine("html", ejs.renderFile);
app.set("view engine", "html");
app.set("views", path.join(__dirname, "public"));

// Middlewares
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Routes
app.use("/", indexRoutes);

app.get("*", (req, res, next) => {
  res.status(404).json("not found 404");
  next();
});

// Static
app.use(express.static(path.join(__dirname, "public")));

// Server
app.listen(app.get("port"), () => {
  console.log(`server on port: ${app.get("port")}`);
});
