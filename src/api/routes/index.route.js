import { Router } from "express";
import indexCtrl from "./../controllers/index.ctrl";

const router = Router();

router.get("/", indexCtrl.index);

router.get("/users", indexCtrl.getUsers);

router.get("/users/:id", indexCtrl.getUserById);

export default router;
