import axios from "axios";

/**
 * @method firstMethod
 * @param {string} text any text do yo want
 * @returns {string}
 */
const firstMethod = (text) => {
  return "hello world";
};

/**
 * Obtener todos los usuarios
 * @method getUsers
 * @async
 * @returns {Object} retorna usuarios
 */
const getUsers = async () => {
  return await axios
    .get(`http://jsonplaceholder.typicode.com/users`)
    .then((response) => res.json(response.data))
    .catch((err) => console.error(err));
};

/**
 * Obtener usuario por ID
 * @method getUserById
 * @async
 * @param {string} userId recibe un string del userId
 * @returns {Object} retorna usurio por ID
 */
const getUserById = async (userId) => {
  return await axios
    .get(`http://jsonplaceholder.typicode.com/users/${userId}`)
    .then((response) => res.json(response.data))
    .catch((err) => console.error(err));
};

/**
 * obtener texto
 * @function getTexto
 * @param {string} text
 * @returns {string}
 */
const getTexto = async (text) => {
  return text;
};
